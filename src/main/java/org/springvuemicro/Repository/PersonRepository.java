package org.springvuemicro.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springvuemicro.Model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person,Long> {

}
